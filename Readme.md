Playground environment for Leaflet coding.


This includes several things:

- a way to position 2D floorplans as on overlay on Leaflet maps

- a marker useful for getting a lat/lon

- D3 pulsing animation used for bluetooth beacons


Change the path on line 68 to another file to manipulate something other than the 30 Rock image.

To manipulate the image, drag the markers (three of them manipulate the image; the pulsing one is just for getting a point). Once the image looks correct, the coordinates for point1 through point3 need to be input into Mongo on a per floor basis and are used to rotate and skew the overlay image accordingly.